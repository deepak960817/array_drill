
function map(elements, cb) {
    
    let outputArray = [];
    
    if(Array.isArray(elements) && elements.length !== 0)
    {
        if(cb && typeof cb == "function")
        {
            for (let index = 0; index < elements.length; index++) 
            {
                if(typeof elements[index] === 'undefined')
                {
                    outputArray.push(elements[index]);
                    continue;
                }
                const value = cb(elements[index], index, elements);
                outputArray.push(value);
            }
        }
    }   

    return outputArray;
}

module.exports = map;