
function reduce(elements, cb, startingValue) {
    
    let output;

    if(elements.length == 0 && typeof startingValue === 'undefined')
    {
        return "Type Error";
    }
    if(Array.isArray(elements) && elements.length == 0 && typeof startingValue === 'number')
    {
        output = startingValue;
        return output;
    }
    if(Array.isArray(elements) && elements.length === 1 && typeof startingValue === 'undefined')
    {
        output = elements[0];
        return output;
    }

    let nextIndex =0;
            
    if(typeof startingValue === 'undefined')
    {
        startingValue = elements[0];
        nextIndex = 1;
    }
    for (let index = nextIndex; index < elements.length; index++) 
    {
        if(typeof elements[index] === 'undefined')
        {
            continue;
        }
        output = cb(startingValue, elements[index], index, elements);
        startingValue = output;
    }
    return output;  
}

module.exports = reduce;