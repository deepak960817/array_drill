
function filter(elements, cb) {
    
    let output = [];
    if(Array.isArray(elements) && elements.length !== 0)
    {
        if(cb && typeof cb == "function")
        {
            for (let index = 0; index < elements.length; index++) 
            {
                if(typeof elements[index] === 'undefined')
                {
                    continue;
                }
                let value = cb(elements[index], index, elements);
                if(value === true)
                {
                    output.push(elements[index]);
                }
            }
        }
    }
    return output;
}

module.exports = filter;