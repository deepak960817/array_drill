const filter = require('../filter.js');

const items = [1, 2, 3, 4, 5, 5];
const words = ['spray', 'limit', 'elite', 'exuberant',, 'destruction', 'present'];
const array = [-3, -2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13];

function isOdd(value, index, elemets)
{
    return value%2 === 1;
}

function greaterthan6(value, index, elemets)
{
    return value.length > 6;
} 

function isPrime(number) {
    for (let index = 2; number > index; index++) {
      if (number % index == 0) {
        return false;
      }
    }
    return number > 1;
  }

const result = filter(array, isPrime);

console.log(result);