
function flatten(elements, depth) 
{
    if(typeof depth === 'undefined')
    {
        depth = 1;
    }
    let output = [];
    if(Array.isArray(elements) && elements.length !== 0)
    {
        for (let index = 0; index < elements.length; index++) 
        {
            if(typeof elements[index] === 'undefined')
            {
                continue;
            }
            if (Array.isArray(elements[index]) && depth >=1) 
            {
                output = output.concat(flatten(elements[index], depth-1))
            } 
            else if (Array.isArray(elements[index]) && depth === 0) 
            {
                output.push(elements[index]);
            }
            else
            {
                output.push(elements[index]);
            }
        }
    }
    return output;
            
  }
  
  
module.exports = flatten;