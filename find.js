
function find(elements, cb) {
    
    let output;
    if(Array.isArray(elements) && elements.length !== 0)
    {
        if(cb && typeof cb == "function")
        {
            for (let index = 0; index < elements.length; index++) 
            {
                let value = elements[index];
                output = cb(value, index, elements);
                if(output === true)
                {   
                    return value;
                }
            }
        }
    }

    return 'undefined';
}

module.exports = find;
