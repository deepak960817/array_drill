
function each(elements, cb) {
    
    if(Array.isArray(elements) && elements.length !== 0)
    {
        if(cb && typeof cb == "function")
        {
            for (let index = 0; index < elements.length; index++) 
            {
                if(typeof elements[index] !== 'undefined')
                {
                    const value = elements[index];
                    cb(value, index);
                }
            }
        }
    }   
}

module.exports = each;